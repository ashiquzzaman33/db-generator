<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'  =>  'Mahmud',
            'email'  =>  'mahmudkuet11@gmail.com',
            'password'  =>  \Hash::make('1234')
        ]);
    }
}
