<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes();

Route::get('/home', ['as'=>'home', 'uses'=>'HomeController@index']);

Route::group(['middleware'=>['auth']], function(){
    Route::resource('category', 'CategoryController');
    Route::get('category/{id}/delete', ['as'=>'category.delete', 'uses'=>'CategoryController@destroy']);

    Route::resource('post', 'PostController');
    Route::get('post/{id}/delete', ['as'=>'post.delete', 'uses'=>'PostController@destroy']);

    Route::resource('media', 'MediaController');
    Route::get('media/{id}/delete', ['as'=>'media.delete', 'uses'=>'MediaController@destroy']);

});