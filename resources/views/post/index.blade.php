@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('partials.error')
                @include('partials.msg')
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li class="active">All Posts</li>
                </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            Post List
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('post.create') }}" class="btn btn-primary btn-xs">Create New</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-heading">
                        <form action="" method="get">
                            <div class="form-group">
                                <label for="category_id" class="col-md-2 control-label">Category</label>

                                <div class="col-md-6">
                                    <select name="category_id" id="category_id" class="form-control input-sm">
                                        <option value="all">All</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"{{ $category->id == request('category_id') ? ' selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="submit" value="Show" class="btn btn-primary btn-sm">
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body table-responsive">
                        <table class="table table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Thumbnail</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 0; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        <td><img src="{{ url($row->thumbnail) }}" alt="" class="img-responsive" style="max-width: 40px;"></td>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->category->name }}</td>
                                        <td>
                                            <a href="{{ route('post.edit', ['id'=>$row->id]) }}" class="btn btn-primary btn-xs">Edit</a>
                                            <a href="{{ route('post.delete', ['id'=>$row->id]) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure want to remove this?')">Remove</a>
                                        </td>
                                    </tr>
                                @endforeach

                                @if($rows->total() == 0)
                                    <tr>
                                        <td colspan="5">No data found!</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>

                        {!! $rows->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
