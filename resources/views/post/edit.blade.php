@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @include('partials.error')
                @include('partials.msg')
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('post.index') }}">Post</a></li>
                    <li class="active">Edit</li>
                </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            Post Edit
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('post.update', $row->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="form-group">
                                <label for="category_id" class="col-md-2 control-label">Category</label>

                                <div class="col-md-10">
                                    <select name="category_id" id="category_id" class="form-control">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"{{ $category->id == $row->category_id ? ' selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-md-2 control-label">Title</label>

                                <div class="col-md-10">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ $row->title }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="excerpt" class="col-md-2 control-label">Excerpt</label>

                                <div class="col-md-10">
                                    <textarea name="excerpt" id="excerpt" rows="5" class="form-control">{{ $row->excerpt }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="thumbnail" class="col-md-2 control-label">Upload Thumbnail</label>

                                <div class="col-md-5">
                                    <img src="{{ url($row->thumbnail) }}" alt="" class="img-responsive">
                                </div>

                                <div class="col-md-5">
                                    <input type="file" name="thumbnail" class="form-control" id="thumbnail">
                                    <small>Thumbnail size: 40 X 40</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="content" class="col-md-2 control-label">Content</label>

                                <div class="col-md-10">
                                    <textarea name="content" id="content" rows="10" class="form-control">{!! $row->content !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

    <script>
        tinymce.init({
            selector: '#content',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [

            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>

@endsection
