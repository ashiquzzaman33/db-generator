@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('partials.error')
                @include('partials.msg')
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li class="active">All Medias</li>
                </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            Media List
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('media.create') }}" class="btn btn-primary btn-xs">Create New</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Copy link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 0; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td> <button class="btn btn-info btn-xs copy_link" data-clipboard-text="{{ $row->link }}">Copy link</button> </td>
                                        <td>
                                            <a href="{{ route('media.edit', ['id'=>$row->id]) }}" class="btn btn-primary btn-xs">Edit</a>
                                            <a href="{{ route('media.delete', ['id'=>$row->id]) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure want to remove this?')">Remove</a>
                                        </td>
                                    </tr>
                                @endforeach

                                @if($rows->total() == 0)
                                    <tr>
                                        <td colspan="4">No data found!</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>

                        {!! $rows->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.13/clipboard.min.js"></script>
    <script type="text/javascript">
        var clipboard = new Clipboard('.copy_link');

        clipboard.on('success', function(e) {
            e.trigger.innerHTML = 'Copied';
            e.trigger.className = 'btn btn-success btn-xs';
            setTimeout(function(){
                e.trigger.innerHTML = 'Copy link';
                e.trigger.className = 'btn btn-info btn-xs';
            }, 2000);
        });

    </script>
@endsection
