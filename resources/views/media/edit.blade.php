@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('partials.error')
                @include('partials.msg')
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('media.index') }}">Media</a></li>
                    <li class="active">Edit</li>
                </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            Edit Media
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('media.update', $row->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Category name</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $row->name }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="file" class="col-md-2 control-label">Upload file</label>

                                <div class="col-md-4">
                                    <img src="{{ url($row->link) }}" alt="" class="img-responsive">
                                </div>

                                <div class="col-md-4">
                                    <input id="file" type="file" class="form-control" name="file">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
