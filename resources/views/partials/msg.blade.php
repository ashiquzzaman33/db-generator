@if(session('msg'))
    <div class="alert alert-dismissible alert-{{ session('type', 'success') }}">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ session('msg') }}
    </div>
@endif