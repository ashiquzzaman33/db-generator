@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('partials.error')
                @include('partials.msg')
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li class="active">All Categories</li>
                </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            Category List
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('category.create') }}" class="btn btn-primary btn-xs">Create New</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 0; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>
                                            <a href="{{ route('category.edit', ['id'=>$row->id]) }}" class="btn btn-primary btn-xs">Edit</a>
                                            <a href="{{ route('category.delete', ['id'=>$row->id]) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure want to remove this?')">Remove</a>
                                        </td>
                                    </tr>
                                @endforeach

                                @if($rows->total() == 0)
                                    <tr>
                                        <td colspan="3">No data found!</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>

                        {!! $rows->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
