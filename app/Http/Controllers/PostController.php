<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();

        if($request->has('category_id') && $request->get('category_id') != 'all'){
            $rows = Post::where('category_id', $request->get('category_id'))->orderBy('id', 'desc')->paginate(10);
        }else{
            $rows = Post::orderBy('id', 'desc')->paginate(10);
        }

        return view('post.index', ['rows'=>$rows, 'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('post.create', ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id'   =>  'required|integer',
            'title'         =>  'required|string',
            'excerpt'       =>  'required|string',
            'thumbnail'     =>  'image',
            'content'       =>  'required|string'
        ]);

        if($request->hasFile('thumbnail')){
            $fileName = str_random(16) . time() .  '.' . $request->file('thumbnail')->getClientOriginalExtension();
            $request->file('thumbnail')->move('uploads', $fileName);
            $thumbnailPath = 'uploads/' . $fileName;
        }else{
            $thumbnailPath = 'uploads/post-default-thumbnail.jpg';    
        }

        Post::create([
            'category_id'   =>  $request->get('category_id'),
            'title'         =>  $request->get('title'),
            'excerpt'       =>  $request->get('excerpt'),
            'thumbnail'     =>  $thumbnailPath,
            'content'       =>  $request->get('content'),
            'total_views'   =>  0,
            'last_viewed'   =>  Carbon::now()
        ]);

        return redirect(route('post.index'))->with('msg', 'Post is created successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Post::find($id);
        $categories = Category::all();
        return view('post.edit', ['row'=>$row, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id'   =>  'required|integer',
            'title'         =>  'required|string',
            'excerpt'       =>  'required|string',
            'thumbnail'     =>  'image',
            'content'       =>  'required|string'
        ]);

        $post = Post::find($id);

        if($request->hasFile('thumbnail')){
            File::delete($post->thumbnail);
            $fileName = str_random(16) . time() .  '.' . $request->file('thumbnail')->getClientOriginalExtension();
            $request->file('thumbnail')->move('uploads', $fileName);
            $thumbnailPath = 'uploads/' . $fileName;
        }else{
            $thumbnailPath = $post->thumbnail;
        }

        $post->update([
            'category_id'   =>  $request->get('category_id'),
            'title'         =>  $request->get('title'),
            'excerpt'       =>  $request->get('excerpt'),
            'thumbnail'     =>  $thumbnailPath,
            'content'       =>  $request->get('content')
        ]);

        return redirect(route('post.index'))->with('msg', 'Post is updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        File::delete($post->thumbnail);
        Post::destroy($id);
        return redirect(route('post.index'))->with('msg', 'Post is removed successfully!');
    }
}
