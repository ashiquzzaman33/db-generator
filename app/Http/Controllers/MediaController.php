<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Media::orderBy('id', 'desc')->paginate(10);
        return view('media.index', ['rows'=>$rows]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required|max:255|string',
            'file'  =>  'required|image'
        ]);

        $fileName = str_random(16) . time() .  '.' . $request->file('file')->getClientOriginalExtension();
        $request->file('file')->move('uploads', $fileName);
        $filePath = 'uploads/' . $fileName;

        Media::create([
            'name'  =>  $request->name,
            'link'  =>  $filePath
        ]);

        return redirect(route('media.index'))->with('msg', 'Media is created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Media::find($id);
        return view('media.edit', ['row'=>$row]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  =>  'required|max:255|string',
            'file'  =>  'image'
        ]);

        $media = Media::find($id);

        if($request->hasFile('file')){
            $fileName = str_random(16) . time() .  '.' . $request->file('file')->getClientOriginalExtension();
            $request->file('file')->move('uploads', $fileName);
            $filePath = 'uploads/' . $fileName;
        }else{
            $filePath = $media->link;
        }


        $media->update([
            'name'  =>  $request->name,
            'link'  =>  $filePath
        ]);

        return redirect(route('media.index'))->with('msg', 'Media is updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::find($id);
        File::delete($media->link);
        Media::destroy($id);
        return redirect(route('media.index'))->with('msg', 'Media is removed successfully!');
    }
}
