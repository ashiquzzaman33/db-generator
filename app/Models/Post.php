<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'category_id',
        'title',
        'excerpt',
        'thumbnail',
        'content',
        'total_views',
        'last_viewed'
    ];
    
    public function category(){
        return $this->belongsTo('App\Models\Category');
    }
}
